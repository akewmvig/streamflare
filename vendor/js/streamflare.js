
// Ready Function
// ===================================================================
$(document).ready(function(){
	$(window).scroll(function(){
		fixedHeader();
	});
	
	$('.btn-video-toggle').click(function() {
		$('.wrapper-vid').toggle();
		$('#video_toggle').toggle();
	});
	
	
});


// Static Function
// ===================================================================
function fixedHeader() {
	var header = document.getElementById("h-head");
	if ($(window).scrollTop() >= 50) {
		$('#h-head').addClass("sticky");
		$('#h-head').css('background', 'rgba(0, 0, 0, 0.5)');
		$('#h-head').css('padding', '5px 0');
	} else {
		$('#h-head').removeClass("sticky");
		$('#h-head').css('background', 'transparent');
		$('#h-head').css('padding', '50px 0');
	}
}